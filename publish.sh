#!/bin/sh
# Publish to appspot, assuming you've installed the Google Cloud SDK.
gcloud --project asciicam --quiet app deploy
echo See it live at https://asciicam.appspot.com/
